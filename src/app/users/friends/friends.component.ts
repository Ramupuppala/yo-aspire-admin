 /*
   *Author      : Rithika
   Date        : 19-03-2018
   Description : service class component for displaying all the friends of the user 
*/

import { Component, OnInit }                  from '@angular/core';
import { UsersService }                       from '../users.service';
import { Router, ActivatedRoute, Params }     from '@angular/router';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})

export class FriendsComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private service: UsersService) { }
  id: number;
  userFriendsData: any;
  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params['id'];
      console.log("ID", this.id);
      this.service.readUser(this.id)
      .subscribe((data: any) => {
        this.userFriendsData = data.user.friend_list;
        console.log(this.userFriendsData.friend_list);
    });
  });
  }

}
