//  Service class component for displaying the users based on search functionality 
import { Component, OnInit }                  from '@angular/core';
import { Router, ActivatedRoute, Params }     from '@angular/router';
import { UsersService }                       from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {

  constructor(private service: UsersService, private router: Router) { }
  userData: string;
  name: string;
  ngOnInit() {
  }
  onChange(event) {
    this.name = event.target.value;
  }

  onSearch() {
    
    if(this.name.length>=4)
    {
      this.service.searchUser(this.name)
      .subscribe((data: any) => {
        console.log(data);
        debugger;
        this.userData = data.results;
      });
    }
    else{
      alert("Please enter more than 3 characters");
    }
  }
}
