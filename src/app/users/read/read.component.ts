/**
 * Author      : Rithika
   Date        : 19-03-2018
   Description : service component class for displaying the complete details of the users 
 *  */
import { Component, OnInit }          from '@angular/core';
import { UsersService }                       from '../users.service';
import { Router, ActivatedRoute, Params }     from '@angular/router';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})
export class ReadComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private service: UsersService) { }
  id: number;
  userDetailData: any;

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.id = params['id'];
      console.log("ID", this.id);
      debugger;
      this.service.readUser(this.id)
        .subscribe((data: any) => {
          debugger;
          this.userDetailData = data.user;
          console.log(this.userDetailData);
        });
    });
  }
}
