/**Routing module for acessing the user functionalities like user read,display.. */

import { NgModule }                             from '@angular/core';
import { Routes, RouterModule }                 from '@angular/router';

import { ReadComponent }                        from './read/read.component';
import { UserDispalyComponent }                 from './user-dispaly/user-dispaly.component';
import { UsersComponent }                       from './users.component';
import { FriendsComponent }                     from './friends/friends.component';

const routes: Routes = [  
  {
    path:"users/read",
    component:ReadComponent
  },
  {
    path: "usersDisplay",
    component: UserDispalyComponent
  },
  {
    path: "users",
    component: UsersComponent
  },
 
  {
    path :"users/friends",
    component : FriendsComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class UsersRoutingModule { }
