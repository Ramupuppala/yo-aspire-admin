
/**
 * Author          :Ramu
  Date             :15-03-2019
  Description      :service class component for displaying all the drives of the user to admin
*/

import { Component, OnInit }            from '@angular/core';
import {DrivesService }                 from '../drives.service';

@Component({
  selector: 'app-my-drives',
  templateUrl: './my-drives.component.html',
  styleUrls: ['./my-drives.component.css']
})

export class MyDrivesComponent implements OnInit {

  constructor( private service:DrivesService) { }
  myDrives:any;
  ngOnInit() {
    this.service.doFetchDrives().subscribe(
      (data)=>{
       
        if(data.statuscode == 1)
        {          
          this.myDrives=data.drives;         
         
        }
      
      }
    )
  }

}
