/* This is the routing module for reading  and to display all the drives of the user*/

import { NgModule }                         from '@angular/core';
import { Routes, RouterModule }             from '@angular/router';
import { DrivesComponent }                  from './drives.component';
import { ReaddrivesComponent }              from './readdrives/readdrives.component';

const routes: Routes = [
    {
        path:"drives",
        component:DrivesComponent
      },
    {
        path:"drives/read",
        component:ReaddrivesComponent
      },
    
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  
  export class DrivesRoutingModule { }
  