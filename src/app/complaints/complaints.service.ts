/*
*Service class for displaying all the complaints raised by the user
 */
import { Injectable }                       from '@angular/core';
import { HttpClient,HttpHeaders }           from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ComplaintsService {
  
  httpOptions: {};
  constructor(private http: HttpClient) {
  }

  getAllComplaints() {
    let token=localStorage.getItem('jwt-token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
      })
    };
    console.log(this.httpOptions);
    return this.http.get<any>('http://13.250.235.137:8050/api/complain', this.httpOptions);
  }
  
}
