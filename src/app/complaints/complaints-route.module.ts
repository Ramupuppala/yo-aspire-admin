/* This is the routing module for displaying all the complaints raised by the user */

import { NgModule }                             from '@angular/core';
import { Routes,RouterModule}                   from '@angular/router';
import { ComplaintsComponent }                  from './complaints.component';

const route:Routes=[
    {
        path:"complaints",
        component:ComplaintsComponent
    }
]
@NgModule({
    imports:[RouterModule.forChild(route)],
    exports:[RouterModule]
})

export class ComplaintsRouterModule{}