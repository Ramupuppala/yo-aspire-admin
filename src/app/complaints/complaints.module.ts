/**
 * ng modules
 */
import { NgModule }                   from '@angular/core';
import { CommonModule }               from '@angular/common';
import { ComplaintsComponent }        from './complaints.component';

import { ComplaintsRouterModule }     from './complaints-route.module';

@NgModule({
  declarations: [
    ComplaintsComponent
  ],
  imports: [
    CommonModule,
    ComplaintsRouterModule
  ],
 exports:[
  ComplaintsComponent
 ]
})

export class ComplaintsModule { }
