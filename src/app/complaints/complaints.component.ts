/*
* Author      : Rithika
  Date        : 16-03-2019
  Description : Component code  for displaying the complaints raised by the user
*/
import { Component, OnInit }          from '@angular/core';
import { ComplaintsService }          from './complaints.service';
import { Router }                     from '@angular/router';

@Component({
  selector: 'app-complaints',
  templateUrl: './complaints.component.html',
  styleUrls: ['./complaints.component.css']
})

export class ComplaintsComponent implements OnInit {

  constructor(private service: ComplaintsService, private router: Router) { }
  complaintsData: any;
  ngOnInit() {
    this.service.getAllComplaints().subscribe(
      (data: any) => {
        console.log(data);
        this.complaintsData = data.complaints_list;
        console.log(this.complaintsData);
      }
    );
  }

}
