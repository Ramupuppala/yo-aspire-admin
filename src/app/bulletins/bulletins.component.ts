/*
* Author      : Rithika
  Date        : 16-03-2019
  Description : Component code  for displaying the created bulletins
*/

import { Component, OnInit }          from '@angular/core';
import { BulletinsService }           from './bulletins.service';

@Component({
  selector: 'app-bulletins',
  templateUrl: './bulletins.component.html',
  styleUrls: ['./bulletins.component.css']
})

export class BulletinsComponent implements OnInit {

 
  constructor(private service: BulletinsService) { }
  bulletinsData: any;
  ngOnInit() {
    this.service.getAllBulletins().subscribe(
      (data: any) => {
        console.log(data);
        this. bulletinsData = data.bulletins;
        console.log(this. bulletinsData);
      }
    );
  }

}
