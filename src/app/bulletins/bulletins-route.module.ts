/* This is the routing module for creating,reading,editing  and to display all the bulletins*/

// component based imports
import { NgModule }                         from '@angular/core';
// routing based imports
import { Routes,RouterModule}               from '@angular/router';
import { BulletinsComponent }               from './bulletins.component';

const routes:Routes=[
    {
        path:"bulletins",
        component:BulletinsComponent
    }
]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})
export class BullietinsRouteModule {}