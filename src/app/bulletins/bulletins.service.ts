/*
*Service class for acessing all the functionalities of the bulletins
like reading displaying all the bulletins
 */
import { Injectable }                       from '@angular/core';
import { HttpClient,HttpHeaders }           from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class BulletinsService {
  
  httpOptions: {};
  constructor(private http: HttpClient) {
  }

  getAllBulletins() {
    let token=localStorage.getItem('jwt-token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
      })
    };
    console.log(this.httpOptions);
    return this.http.get<any>('http://13.250.235.137:8050/api/bulletins/all', this.httpOptions);
  }
  
}
