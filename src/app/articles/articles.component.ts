/*
* Author      : Rithika
  Date        : 16-03-2019
  Description : Component code  for displaying the articles*/

// component based imports
import { Component, OnInit } from '@angular/core';
// service based imports
import { ArticlesService } from './articles.service';
// routing based imports
import {Router} from '@angular/router';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})

export class ArticlesComponent implements OnInit {

  constructor(private service:ArticlesService,private router: Router) { }
  articles:any;
  searchKey:string;
  ngOnInit() {
    this.service.getAllArticles().subscribe(
      (data)=>{
        console.log(data)
        if(data.statuscode == 1)
        {
          this.articles=data.articles
         
          console.log("success"+this.articles);
        }
      
      }
    )
  }
  onChange(e)
  {
    this.searchKey=e.target.value;
  }
  onSearch(){   
    console.log(this.searchKey)
    if(this.searchKey.length>=4)
    {
      this.service.searchArticle(this.searchKey).subscribe(
        (data)=>{
          console.log(data)
          if(data.statuscode == 1)
          {
            this.articles=data.articles
          
            console.log("success"+this.articles);
          }
        
        }
      )
    }
    else{
      alert("Please enter more than 3 characters");
    }
  }
  
}
